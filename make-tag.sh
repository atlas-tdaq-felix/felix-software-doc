#!/bin/sh
set -e

if [ $# -ge 1 ]; then
  TAG=${1}
  shift
else
  echo "Usage: make-tag.sh <tag>"
  exit 1
fi

# Check if tag exists
if [ $(git tag -l "${TAG}") ]; then
    echo "Tag already exists"
    exit 1
fi

# Create the tag and commit
./make-html.sh ${TAG}
./make-pdf.sh ${TAG}
git commit -a -m "Added ${TAG}"
git tag ${TAG}

# Put branch back and commit
./make-html.sh
./make-pdf.sh
git commit -a -m "Set back branch"

# Show log
git log -n 5

# Push all
git push
echo "Waiting 15 seconds..."
sleep 15
git push --tags


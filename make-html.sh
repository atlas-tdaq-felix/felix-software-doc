#!/bin/sh
set -e

if [ $# -ge 1 ]; then
  BRANCH=${1}
  shift
else
  BRANCH=`git rev-parse --abbrev-ref HEAD`
fi

#
# Update the antora, antora-playbook
#
sed -e "s/<branch>/${BRANCH}/g" antora.in.yml > antora.yml
sed -e "s/<branch>/${BRANCH}/g" antora-playbook.in.yml > antora-playbook.yml
antora antora-playbook.yml --stacktrace --log-level debug --log-failure-level error
find build

To generate HTML/PDF for this documentation, install

```
gem install asciidoctor asciidoctor-pdf
```

then run for HTML or PDF

- ./make-html.sh
- ./make-pdf.sh

Mark Donszelmann

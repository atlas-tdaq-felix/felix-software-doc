# Specifications

FELIX Bus supports communication between different instances of FELIXCore
and their clients, such as the SwROD[1].

FELIX Bus has the following features:

* FELIX Bus can have any number of clients. Clients can be publishers of information,
subscribers of information or both.

* Information is published on the FELIX Bus, which is distributed to
all subscribers on the FELIX Bus.

* When a publisher updates information on FELIX Bus, all subscribers are notified.

* Subscribers can do lookups of information at any time.

* When a subscriber unsubscribes from FELIX Bus, intentionally or due to failure,
the information published by this subscriber is retracted from FELIX Bus
and other subscribers are informed.

* Within Felix the FELIX Bus is used to distribute tables with E-Link->host/port
association as well as tables for monitoring purposes.

* There is no single point of failure within FELIX Bus, it exists with its information
as long as there is at least one client.

* FELIX Bus supports auto discovery of clients within a network.

# Implementation

FELIX Bus is a virtual bus. Any client just needs to link with the
FELIX Bus library, and publish information and or subscribe to information.
FELIX Bus is a thin layer on top of ZeroMQ[2] and Zyre[3].

FELIX Bus announces its client via the Zyre beacon system, using a network broadcast.
Internally this uses the UDP protocol.
Each client is notified of the arrival or departure of any other client, within the
network.

Once a client is known, further communication takes place via ZeroMQ, using the
TCP/IP protocol. The ZeroMQ publish/subscribe mechanism is used to distribute
information between different clients. A new client requests each other client for
its information, thus building the full set of information. Each client replies
on these requests.

# FELIX usage of FELIX Bus

In FELIX the FELIX Bus is used for distribution and synchronization  of two maps:

* ElinkTable, which links **E-Link** to **FelixId** and **peerId**
* FelixTable, which links **FelixId** to **host/port** and **peerId**

in which:

* **elink** is a 11 bit E-Link or link ID used inside Felix.
* **felixId** is a unique id or address per FELIX Core instance. *This needs to be sorted out still with regards to the Global FELIX ID proposal*
* **host/port** is the address where a Felix client needs to connect to.
* **peerId** is an internal ZeroMQ/Zyre ID to use when a client fails or disappears.

These two FELIX tables allow lookup of E-Link->host/port information.

FELIX Bus handles the failure of a client by removing the entries with a particular peerId
from any of the tables used.

# Performance

No performance measurements have been done.

# Issues and Improvements

The following improvements may be implemented:

* Proper API to deal with both the publishing of E-Links and addresses as well
as their lookups, without having to deal with the two tables, which are used
internally in FELIX Bus.
* More information about the E-Links (e.g. width) may be published.
* The actual list(s) may be too large to fully distribute to all clients.
To avoid this one could foresee 2 or 3 servers, of which the hostnames are
distributed via FELIX Bus. Theses servers would contain the full tables,
which are partially handed to them via the publishing clients. Each of the
publishing clients would thus only distribute their partial tables to 2 or 3
servers. Subscribing clients would be notified when the tables change, and
would do single or multiple lookups of limited number of E-Links.

# References

1. SwROD, *ATLAS Software ROD*, []()
2. ZeroMQ, *Distributed Messaging*, [http://zeromq.org](http://zeromq.org)
3. Zyre, Local Area Clustering for Peer-to-Peer Applications, [https://github.com/zeromq/zyre](https://github.com/zeromq/zyre)

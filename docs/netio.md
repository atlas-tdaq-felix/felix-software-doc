# Specifications

## Communication Patterns

The first communication pattern in NetIO is simple point-to-point communication.
In this pattern a sender sends a series of messages to a receiver. The
established connection is reliable and preserves order of messages. NetIO
sockets are used to establish connections and send messages. A send socket can
connect to exactly one receive socket. A receive socket can receive messages
from multiple send sockets. The send/receive pattern is illustrated in the
following figure.

![Send/Receive](SendReceive.svg)

The second communication pattern is publish/subscribe communication.
A publish/subscribe system, as illustrated in
the Figure below, has two types of actors: subscribers, which receive
data, and publishers, which publish data. The publisher does not have any
prior knowledge of the subscribers. Instead, subscribers send a subscription
request to the publishers. Publishers receive the subscription request and
add the subscriber to a subscription table. When publishing a message, a
publisher looks up the subscription in the subscription table and sends the
message to all subscribers. A subscriber can specify in the subscription
request a mask to filter the messages that it will receive.

![Publish/Subscribe](PublishSubscribe.svg)

## Communication Modes

NetIO distinguishes between two principal communication modes: buffered
high-throughput communication and unbuffered low-latency communication. The
relevant send and receive sockets operate differently in several aspects
including the use of threads, use of the event loop, use of callbacks and use of
buffers.


## Back-End System

NetIO has a back-end system to support different network technologies and APIs.
Currently two different back-ends exist. The first back-end uses POSIX sockets
to establish reliable connections between endpoints. Primarily this back-end is
used on TCP/IP connections in Ethernet networks, but in principle a technology
like SDP could also be used. The second back-end is based on libfabric. It is
used for communication via Infiniband as well as RDMA-over-Ethernet
technologies.


# Implementation

## Architecture

The NetIO architecture is illustrated in the following Figure.

![NetIO Architecture](NetIOArchitecture.svg)

There are two software layers within NetIO. The upper level contains
user-level sockets. These are the sockets that application code interacts
with. The different socket types are listed in Table~\ref{table:sockets}.

The lower architecture level provides a common interface to the underlying
network APIs. The common interface consists of three low-level socket types (a
send socket, a listen socket and a receive socket), which are implemented by
each back-end. The low-level sockets provide basic connection handling and
simple transmission of messages between two endpoints. All higher level
functionality like buffering, notification of user code via callbacks, or the
publish/subscribe system are implemented in the user-level sockets. This
maximizes code sharing among the back-ends, as only code that is specific to the
underlying network technology is implemented in the low-level sockets.

Both architecture levels use a central event loop to handle I/O events like
connection requests, transmission completions, error conditions, or timeouts.
The event loop is executed in a separate thread. Its implementation is based on
the epoll framework in the Linux kernel.

Network endpoints are addressed by IP address and port, even for
back-ends that do not natively support this form of addressing. For the
Infiniband back-end the librdma compatibility layer is used to enable addressing
by IP and port.


## High-Throughput and Low-Latency Sockets

High-throughout sockets and low-latency sockets work fundamentally different.
The differences are described in this section.

A high-throughput send socket does not send out messages immediately but
maintains buffers in which messages are copied. The buffers are referred to as
pages. Due to the buffering fewer, larger packets are sent on the network
link. This approach is more efficient and yields a higher throughput. However, the
average transmission latency of any specific message is increased due to
the buffering. Once a page is filled the whole
page is sent to the receiving end. Additionally, a timer (driven by the
central event loop) flushes the page at regular intervals to avoid starvation
and infinite latencies on connections with a low message rate. A typical timeout
interval is 2\,s. A message is split if it does not fit into a single page.
The original message is reconstructed on the receiving side.

A high-throughput receive socket receives pages that contain one or more
messages or partial messages. The messages are encoded by simply prepending an
8\,byte length field to the message contents. The high-throughput receive socket
maintains two queues: a page queue, which contains unprocessed pages that have
been received from a remote, and a message queue, in which messages are stored
that are extracted from pages when they are processed. The high-throughput
receive socket enqueues received pages in the receive page queue. When user code
calls \texttt{recv()} on a high-throughput receive socket, it will return the
next message from the message queue. If the message queue is empty, the next
page from the receive page queue is processed and the contained messages are
stored in the message queue. When processing a received page the contained
messages are copied. A new zero-copy mode is currently under development in
which it is also possible to avoid this additional copy.

A low-latency send socket does not buffer messages. Messages are immediately
sent to the remote process. Unlike for high-throughput send sockets, there is
also no additional copy: the message buffer is directly passed to the
underlying low-level socket. These design decisions minimize the added
latency of a message send operation.

A low-latency receive socket handles incoming messages by passing them to the
application code via a user-provided callback routine, instead of enqueuing the
messages in a message queue. This approach enables incoming messages to be
processed immediately. The message buffer is only valid during execution of the
callback. After execution of the callback routine the receive
buffer will be freed. If necessary, a user can decide to copy the buffer in the
callback routine.



<!---
![NetIO High-Throughput Mode](NetIOThreading-HighThroughput.svg)
![NetIO Low-Latency Mode](NetIOThreading-LowLatency.svg)
-->

# Performance

![NetIO Throughput](NetIO-Throughput.svg)
![NetIO RTT](NetIO-RTT.svg)



# Issues and Improvements

# References

1. Jörn Schumacher, *Utilizing HPC Network Technologies in High Energy Physics Experiments*, [available on CDS](https://cds.cern.ch/record/2273702)
2. Jörn Schumacher, *Interfacing Detectors and Collecting Data for Large-Scale Experiments in High Energy Physics Using COTS Technology*, PhD Thesis, [available on CDS](https://cds.cern.ch/record/2268506)

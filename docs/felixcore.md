# Specifications

The FELIX Core application is the central process of a FELIX system.
It handles the communication between one or more FELIX Cards on one side and a set of [NetIO](netio) clients on the other side. FELIX Core is a stateless system, to be run all the time, for DAQ and DCS purposes.
It runs on a PC, running the Linux operating system, in which the FELIX Card(s) and Network Card(s) are installed.

FELIX Core has the following functions:

1. Packet forwarding from the front-ends to the DAQ system: read and decode data packets from the FELIX Card and forward them as messages to NetIO clients, based on dynamic routing rules.
2. Packet forwarding from the DAQ system to the front-ends: receive messages from NetIO clients and forward them as packets to the FELIX Card E-Links as supplied in the message header.
3. Configure the FELIX Card, E-Link configuration and operational parameters based on the input of a configuration file.
4. Recover from host failures. Using a publish/subscribe system host failures of FELIX clients can be handled transparently.
5. Advertise E-Link meta information to NetIO clients before they actually connect. _Use the Global FELIX ID._
6. Gather statistics and performance metrics and make those available.
7. Report operational status information, such as the status of the detector links, and warn in the case of hardware failure.
8. Offset E-Links to distinguish when multiple FELIX Core applications are run to read out multiple cards. _Use the Global FELIX ID._
9. Handle the BNL-711 card, which internally is seen as two cards, by a single FELIX Core instance.
10. _Handle multiple FELIX Cards (including BNL-711) by the same FELIX Core instance._

![FELIX Core Architecture](FELIXCoreArchitecture.svg)

The figure shows a diagram of the architecture of the FELIX Core application. In the case of a system with multiple FELIX Cards, multiple FELIX Core applications are run.

## FELIX Card Interface

A FELIX Card can run or be programmed in GBT or in FULL Mode. For FELIX Core the handling of GBT or FULL Mode makes no difference other than that in GBT mode Links are made of GBT/E-Group/E-Path combinations and in FULL mode Links are just Links. In the following the word E-Link (GBT Mode) is used synonymously with FULL mode link.

Per FELIX Card, and in the case of the BNL-711 per end-point on the card, 11 bits are reserved for the E-Link ID providing for a maximum of 2048 E-Links.
_A specification of a Global FELIX ID, which incorporates the E-Link, is being prepared, to allow each subsystem to have their own range of E-Links._

Communication with the FELIX Card is done by using a driver. The driver gives access to:

- a register map of the card, for status and control, see Register Map[1]
- two DMA channel for fast data transfer in and out of the card, one DMA channel each
- interrupts to react to the cards needs

To make it easier to interface with the three items above the FLX Card API was created.

## NetIO Interface

Communication with [NetIO](netio) is done via the NetIO Library which supplies:

- a publish-subscribe model, allowing clients to subscribe to certain data on a node/port combination
- low-latency and high-throughput data communication

## FELIX Bus Interface

At the startup of FELIX Core a list of E-Links and host/port combinations will be published in a set of tables on [FELIX Bus](felixbus). Any FELIX Bus client, including any NetIO client, finds E-Links by subscribing to the FELIX Bus and looking up the E-Link in the table to find out where to connect.

## Input and Output Formats

Input into and Output from FELIX Core, both on the FELIX Card and NetIO side, is documented in the following formats:

| Name                                           | Type         | Description                 |
|:-----------------------------------------------|:-------------|:----------------------------|
| [FELIX To Host](formats/FelixToHostFormat)     | DAQ, DCS/SCA | Reading from the FELIX Card |
| [FELIX From Host](formats/FelixFromHostFormat) | DCS/SCA      | Writing to the FELIX Card   |
| [FELIX Input](formats/FelixInputFormat)        | DCS/SCA      | Input from FELIX Clients    |
| [FELIX Output](formats/FelixOutputFormat)      | DAQ, DCS/SCA | Output to FELIX Clients     |

## To Host Data Handling

Data is read from the card, via DMA into the memory of the PC, and then routed according to E-Link to
NetIO subscribers. ToHost data (chunks) are split, by the FELIX firmware, into sub-chunks to fit in 1024 Byte blocks with an E-Link and Sequence number in the header.
FELIX Core reads blocks, sorts them by E-Link, checks Sequence numbers, and recombines sub-chunks into chunks of data.
These chunks of data are subsequently published via NetIO, prepended with the FELIX header.
Blocks in the ToHost direction can contain any E-Link, meaning there is no order of E-Links, but blocks of one E-Link must arrive in order with Sequence number incremented by one.

## From Host Data Handling

NetIO Clients may send data to FELIX Core over NetIO, which is then copied to the card using DMA. Upon arrival of a message in NetIO the header is inspected for the E-Link and data is sent to the respective E-Link. Data sent to FELIX Core has to be encoded as 32 byte packets with a 2 byte header, which specifies the E-Link.

## Monitoring

Monitoring information is made available in JSON format[2] using an internal web server. A set of simple
web pages can display this information by connecting to a FELIX Core instance. Programs such as 'felix-mon'
connect to multiple FELIX Core instances, provide history and forward monitoring information to
the ATLAS DAQ system.

## Error Handling

_Errors in the data stream are signaled as part of the sub-chunks, see [FELIX To Host Format](formats/FelixToHostFormat).
Errors, such as CRC and 8B/10B encoding errors, are forwarded into the status field of the FELIX Header, see [FELIX Output Format](formats/FelixOutputFormat).
The same information is also published on a virtual ERROR E-Link._

## BUSY Handling

_The handling of the BUSY both in Firmware and Software is under [discussion](https://its.cern.ch/jira/browse/FLX-480)._
_A BUSY can be raised by software by setting the correct register in the FELIX Card.
This is combined with the BUSY of the card itself and output on the BUSY Lemo connector.

FELIX Core raises the BUSY if one of the following conditions is met:

1. A BUSY signal was raised by at least one of the NetIO clients (e.g. SWROD). This signal is received via a separate virtual BUSY E-Link.
2. The internal output buffers towards NetIO are getting full. In this case enough space should be
available after raising the BUSY to handle all incoming data until the front-ends stop sending data.

To identify who raised the BUSY on the Card, FELIX Core provides a virtual BUSY Info E-Link which shows the origin of the BUSY signal.
In the case of internal buffers running full FELIX Core could possibly provide a list of E-Links._

## L1A Handling

L1A information coming in via the TTC cable on the FELIX card can be routed to any E-Link. FELIX Core merely
routes the information to any NetIO subscribers. _FELIX Core configures the E-Link number and its width._

In the FELIX Tool set a separate application called 'elinkconfig'[3] can be used to handle settings of the FELIX Card, such as E-Link configuration.

Currently 'elinkconfig' handles all configuration.

## Filtering

_Filtering of ToHost traffic is foreseen, but has not been specified nor implemented._

## Handling of Stream IDs

_The handling of StreamIDs both in Firmware and Software is under [discussion](https://its.cern.ch/jira/browse/FLX-590)._

## Configuration

All E-Links will be configurable by FELIX Core. A FELIX ID scheme and the configuration of E-Links is under [discussion](https://its.cern.ch/jira/browse/FLX-593).
Configuration will happen when the FELIX Core is started.

Currently 'elinkconfig' handles all configuration.

# Implementation

## Initialization

The initialization of the FELIX Cards is done by FELIX Core. __At this time any real initialization of links is done using 'elinkconfig'.__

## Data handling

In FELIX Core the handling of To Host and From Host is entirely separated and described below.

### To Host

The DMA of the FELIX Card is setup in continuous mode, filling a contiguous buffer of memory, allocated by the cmem_rcc driver[4].
A single thread is responsible for reading data from the contiguous memory. Data is encoded in 1024 Byte blocks. The header of a block is inspected for its E-Link and sequence number. The sequence number is verified not to be out of order. Track is kept in a table of E-Links and current sequence numbers. Based on the E-Link a block is copied to one of multiple block queues. The same E-Link always goes to the same block queue. For each block queue a worker thread handles the processing of the blocks. Within the worker thread the blocks will run through a pipeline using the following steps:

1. The block is copied from contiguous memory into a buffer. This buffer is pre-allocated in the worker thread and retrieved from form a queue of free blocks. At this point the memory in the contiguous buffer can be made available for further DMA transfers by advancing the read pointer.
2. The next step is to decode the block into variable-length packets also known as chunks. Sub-chunks of in previous blocks are prepended to the current sub-chunk to create a data chunk, if possible, or the block is kept to wait for more data to come in.
3. The next action is to gather statistics. This step counts sub-chunks and some other entities processing it into some central data structure, which uses atomic variables to ensure thread synchronization. Histograms are also built using special queues to make sure they have no impact on overall system performance. All this information including configuration of FELIX Core itself is made available to FELIX Monitoring.
4. In this step information is extracted from the chunks. This information is used to route the data packets in the next step. At this time only the E-Link is stored. In future this could be extended to extract an event identifier or other information.
5. The meta information from the previous step is used to assign one or more NetIO clients for the chunk.
6. In this final step the chunk is sent to the NetIO clients assigned in the previous step.

Copies of data, which happen to be expensive, happen in the first and last step of the process. The first step is needed to free up, as fast as possible, the contiguous buffer of memory the DMA writes into. In the last step a copy is needed to transfer data to the underlying network stack. The second copy can be avoided by using NetIO's RDMA capabilities.

For testing purposes the FELIX Card reader can be replaced at runtime by a file reader, which reads data from a file or a data generator, which generates random data. In both cases FELIX Core will see this data as if it was read from a FELIX Card.  

### From Host

Traffic that is sent from NetIO Clients to the frontend is handled by a single thread. Messages from NetIO Clients need to be encoded as 32 bytes including a 2 byte header. The header is decoded and the data is routed to the respective E-Link.
HDLC encoding of data is to be done on the NetIO client side with a special library, which is a deliverable of the FELIX project and is available. FELIX Core is not aware of the fact that an E-Link is encoded with HDLC. FELIX Core monitors the number
of blocks that are sent to each E-Link. This information is available via the FELIX monitoring tools and the FELIX Core web frontend.

## Monitoring

Statistics and configuration information is made available in JSON format by FELIX Core. Internally it runs a small web-server which supplies this information on request. For testing purposes FELIX Core provides a few web pages displaying this information, but as the web server is fairly simple, one should restrict the number of web clients to one or two.

For extensive monitoring of FELIX hosts we have multiple tools:

1. The tool 'felix-mon' to log FELIX data metrics to disk
2. A standalone web application can display current and historical monitoring metrics of FELIX hosts in the same network
3. An integration of FELIX with the TDAQ PBEAST database, that essentially connects FELIX to the TDAQ monitoring infrastructure.

# Performance

TBD - Full-chain measurements ongoing.

# Issues and Improvements

The following issues and possible improvements are known:

1. Busy handling needs to be implemented.
2. Error handling needs to be implemented.
2. Filtering needs to be specified and implemented.
3. E-Link StreamIDs need to be specified and implemented.
4. FELIX Core does not handle configuration, 'elinkconfig' is currently used to do so. We may want to take code from 'elinkconfig' and create a library for configuration. This library can then be used by 'elinkconfig' and FELIX Core for doing the configuration.
5. Lockout may occur if in the FromHost direction large data is sent (split in 32 byte packets) to a single E-Link. As there is currently just one thread handling this direction, we may be spending too much time on large data. We would better use some round-robin algorithm to handle queues from different NetIO subscribers, not to lockout anyone.

# References

1. FELIX Register Map, [https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/regmap/registers.pdf](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/regmap/registers.pdf)
2. JSON Format, [http://www.json.org](http://www.json.org)
3. FELIX User Manual, [https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/docs/FelixUserManual.pdf](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/docs/FelixUserManual.pdf)
4. FELIX Driver, [https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/docs/felix_driver.pdf](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/docs/felix_driver.pdf)

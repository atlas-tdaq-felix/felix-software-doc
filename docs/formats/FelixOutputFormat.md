# FELIX Output Format

Each data chunk sent over NetIO has the following header, followed by the data chunk:

FromFELIXHeader, 8 Bytes

| Bits  | Length | Description |
|:------|:-------|:------------|
| 0-15  | 16     | Length      |
| 16-31 | 16     | Status      |
| 32-37 | 6      | E-Link ID   |
| 38-63 | 26     | GBT ID      |

# FELIX Input Format

Data sent to FELIX over NetIO should start with the following header:

ToFELIXHeader, 16 Bytes

| Bits   | Length | Description |
|:-------|:-------|:------------|
| 0-31   | 32     | Length      |
| 32-63  | 32     | Reserved    |
| 64-95  | 32     | E-Link ID   |
| 96-127 | 64     | Reserved    |

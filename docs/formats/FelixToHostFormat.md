# FELIX To Host Format

The ToHost format, previously documented under [PCIe Bus Data Transfer Format](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/PCIeBusDataTransferFormat),
is a 1024 Byte Block format described below:


## Block Format

A block has a fixed size of 1024 Bytes, including a 4 Byte header. The payload of a block is thus 1020 Bytes long.
The block header is defined as follows:

| Bits  | Length | Description                    |
|:------|:-------|:-------------------------------|
| 0-2   | 3      | E-Path                         |
| 3-5   | 3      | E-Group                        |
| 6-10  | 5      | GBT                            |
| 11-15 | 5      | Sequence Number                |
| 16-31 | 16     | Start-of-Block Symbol (0xABCD) |

in C:
```C++
struct block {
   uint32 elink : 11
   uint32 seqnr :  5;
   uint32 sob   : 16;
   char   data[1020];
};
```

* The fields GBT, E-Group and E-Path together identify an E-Link.
* The Sequence Number is incremented by one for every block for a particular E-Link. It wraps around after 32 blocks.


## Sub-chunk Format

A chunk is an arbitrary length packet, which is packed into blocks. A
chunk can span multiple blocks, in which case it is split into sub-chunks. Each
sub-chunk has a 2 Byte trailer, but no header. Sub-chunks are Byte-aligned. The
trailer format is:

| Bits  | Length | Description                                                                  |
|:------|:-------|:-----------------------------------------------------------------------------|
| 0-9   | 10     | Length (in bytes)                                                            |
| 10    | 1      | Reserved for Length Field Extension                                          |
| 11    | 1      | Chunk Error                                                                  |
| 12    | 1      | Truncation (received more data for this chunk than the maximum chunk length) |
| 13-15 | 3      | Sub-chunk Type                                                               |

in C code:
```C++
struct subchunk_trailer
{
  uint16 length   : 10;
  uint16 reserved :  1;
  uint16 err      :  1
  uint16 trunc    :  1
  uint16 type     :  3;
};
```
### Length

The length of the sub-chunk is specified in Bytes. The length of the trailer is not included.
Thus, the maximum length is 1018 Bytes (1020 - 2 Bytes), while the minimum length is 0 Bytes.
Padding bytes (see [padding](#Padding).) which might occur between sub-chunk data and trailer
do not contribute to the sub-chunk length, specified in the length field.
If the sub-chunk length is odd, a padding byte is inserted to make it even.

### Chunk Error

The error bit is set in case an error has been detected in the current chunk.
An error could for example be a CRC error or indicate bad 8B/10B encoding.

CRC errors only are signaled for FULL mode links, as the FULL mode protocol defines a CRC check.
CRC errors are indeed signaled with the 'err' bit.

8B/10B encoding errors (two SOPs without EOP after the first SOP or two EOPs without SOP after the first EOP, i.e. framing errors)
are signaled as subchunk-type 6.

### Truncation

The truncation bit is set when more data for a chunk is received than it has
been specified as maximum chunk length.


### Sub-chunk Type

The sub-chunk type is a 3-bit encoding according the following mapping:

| Name        | Value   | Description                                                                                                                  |
|:------------|:--------|:-----------------------------------------------------------------------------------------------------------------------------|
| null        | 0=0b000 | SubChunk is used to fill up a non-full block                                                                                 |
| first       | 1=0b001 | SubChunk marks the beginning of a chunk                                                                                      |
| last        | 2=0b010 | SubChunk marks the end of a chunk                                                                                            |
| both        | 3=0b011 | SubChunk is a whole chunk, i.e. chunk fits into block                                                                        |
| middle      | 4=0b100 | SubChunk is a continuation of a previously started chunk, and more SubChunk will follow                                      |
| timeout     | 5=0b101 | No data received for some given time                                                                                         |
| error       | 6=0b110 | 8B/10B errors -                                                                                                              |
| out-of-band | 7=0b111 | Out-of-band message. Implies no payload data. The rest of the trailer bits is used to indicate the out-of-band message type. |

* Out-Of-Band Messages: This type of sub-chunk trailer is used to carry status or error information to the host. No payload data is carried; thus, the implied sub-chunk length is 0. The rest of the bits in the trailer (length field, truncation bit, ...) can be used freely to indicate error types, status codes, etc.

## Padding

Since a sub-chunk is at least 2 byte long (sub-chunk trailer size), but the
sub-chunk length is not guaranteed to be a multiple of 2 byte, it is not always
possible that the block payload can be completely filled with sub-chunks. In
that case, padding bytes have to be added.

### Enforce 16-bit alignment of sub-chunks

Every sub-chunk is placed at a 16-bit boundary. Additionally, every sub-chunk
trailer is placed at a 16-bit boundary. In case the sub-chunk length is not a
multiple of 2 byte, one padding byte is added between the sub-chunk
payload and the sub-chunk trailer:

```text
   1  |  0  |  1  |  0  |  1  |  0  |  1  |  0  |  1  |  0  |  1     
------ ----------- ----------------- ----- ----------- -----------------
 ...  |  Trailer  |   Subchunk Data | PAD |  Trailer  | Subchunk Data ..
------ ----------- ----------------- ----- ----------- -----------------
                  ^                    ^   \-------- Trailer starts at
                Subchunk starts at   Padding byte    2 byte boundary
                2 byte boundary      is added
```

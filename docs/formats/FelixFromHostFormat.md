# FELIX From Host Format

Data is sent to the FELIX Card as packets of 32 bytes, starting with a header of 2 bytes,
and a payload of 30 bytes.

FromHostHeader, 2 Bytes

| Bits  | Length | Description                                       |
|:------|:-------|:--------------------------------------------------|
| 0     | 1      | End-Of-Message, set to 1 for last part of message |
| 1-4   | 4      | Length in 2 byte words                            |
| 5-7   | 3      | E-Path                                            |
| 8-10  | 3      | E-Group                                           |
| 11-15 | 5      | GBT                                               |

* Length is specified in 2 byte words, so length in bytes is always even.

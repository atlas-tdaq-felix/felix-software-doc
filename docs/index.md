# FELIX Software Documentation

This document describes for each of the components in a running DAQ/DCS
system:

* Specifications
* Implementation
* Performance
* Issues and Improvements

The following components are listed:

* [FELIX Core](felixcore), the main application to communicate between FELIX Clients and FELIX Cards
* [FELIX Bus](felixbus), a communication bus for control messaging between FELIX Core instances and their FELIX Clients
* [NetIO](netio), a fast communication protocol for data messaging between FELIX Core and FELIX Clients

Any notes in Italics are not yet implemented.

A [PDF](felix-doc.pdf) version is available.
